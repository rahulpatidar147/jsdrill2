function map(elements, cb){
    let resultAarray = [];
    for(let index = 0; index <elements.length ;index++){
        resultAarray.push(cb(elements[index]));
    }
    return resultAarray;
}
module.exports = map;


