const map = require('../map.js');
const items = [1, 2, 3, 4, 5, 5];
const sqrtFun =(value)=>{
    return value*value;
}
let result=map(items,sqrtFun);
let originalResult=items.map(sqrtFun);
console.log(originalResult);
console.log(result);
let elementsMatch =true;
for(let index=0;index<originalResult.length;index++){
    if(result[index]!==originalResult[index]){
        elementsMatch = false;
        break;
    }
}
elementsMatch ? console.log("Result are match"):console.log("Result are not match");
