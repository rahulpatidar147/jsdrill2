const each = require('../each.js');
const items = [1, 2, 3, 4, 5, 5];
const eachValueFn=(value)=>console.log(value);
function eachValueWithIndex(value,index){
    console.log(value , index);
}
each(items,eachValueFn);
each(items,eachValueWithIndex);