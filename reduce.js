
function reduce(elements, cb, startingValue=0) {
    let sum =0;
    for(let index=startingValue;index<elements.length;index++){
        startingValue=sum;
        sum=cb(startingValue,elements[index]);
        
    }
    
    return sum;
}
module.exports = reduce;